class ClassList:
    def __init__(self):
        self._class_list = []
    
    def __iter__(self):
        self.__i = 0
        return self
    
    def __next__(self):
        if self.__i >= len(self._class_list):
            raise StopIteration
        
        x = self.__i
        self.__i += 1
        return self._class_list[x]
    
    def add_class(self, class_name):
        self._class_list.append(class_name)
    
    def list_classes(self):
        return self._class_list
