class School:
    def __init__(self):
        self._class_list = []
        self.name = ""
    
    def __str__(self):
        print("{}".format(self.name))
    
    def add_class(self, class_name):
        self._class_list.append(class_name)
    
    def list_classes(self):
        return self._class_list

class High(School):
    def __init__(self):
        super(High, self).__init__()
        self.name = "High School"
        
class Middle(School):
    def __init__(self):
        super(Middle, self).__init__()
        self.name = "Middle School"
        
class Elementary(School):
    def __init__(self):
        super(Elementary, self).__init__()
        self.name = "Elementary School"
