import json

def interpret_json_tree(open_json_file):
    return json.load(open_json_file)

def write_json_tree(python_tree, open_file):
    json.dump(python_tree, open_file)
    
def print_level(partial_tree):
    later = []
    for node in partial_tree:
        if isinstance(node, list):
            later.extend(node)
        else:
            print(node, end = " ")
        
    print()
    
    if later:
        print_level(later)

def print_tree(python_tree):
    if len(python_tree) == 0:
        print("empty tree")
        return
    
    print_level(python_tree)

def get_children_from_user(name):
    new_level = input("does this node ({}) have children? yes or no: ".format(name))
    if "y" in new_level:
        children = []
        child = input("input child node name ")
        tree_rooted_at_child = [child]
        tree_rooted_at_child.extend(get_children_from_user(child))
        children.append(tree_rooted_at_child)
        children.extend(get_sibling_from_user(child))
        return children
    else:
        return []
        
        
def get_sibling_from_user(name):
    new_sibling = input("does this node ({}) have siblings? yes or no: ".format(name))
    if "y" in new_sibling:
        siblings = []
        sib = input("input sibling node name ")
        tree_rooted_at_sibling = [sib]
        tree_rooted_at_sibling.extend(get_children_from_user(sib))
        siblings.append(tree_rooted_at_sibling)
        siblings.extend(get_sibling_from_user(name))
        return siblings
    else:
        return []

def get_tree_from_user():
    tree = []
    answer = input("input root node name ")
    tree.append(answer)
    tree.extend(get_children_from_user(answer))
    return tree

def main():
    sample_tree = ["l1", ["l2", "l3", ["l4", "l5"], "l6", ["l7"]]]
    print("my tree")
    print_tree(sample_tree)
    with open("tree_dump.txt", "w") as out_file:
        write_json_tree(sample_tree, out_file)
    
    with open("tree_dump.txt", "r") as in_file:
        loaded_tree = interpret_json_tree(in_file)
    
    if loaded_tree:
        print("my tree, saved and loaded")
        print_tree(loaded_tree)
    
    user_tree = get_tree_from_user()
    print("user tree v1")
    print(user_tree)
    print("user tree v2")
    print_tree(user_tree)
    
main()
