# dorothy carter - august 2020

def index_to_chr(i):
    return chr(i + ord('a'))

def find_sum_indices(str):
    total = 0
    for ch in str:
        total += (ord(ch) - ord('a'))
    
    return total

def char_that_completes_2l_str(mod, str_so_far):
    if mod == 0:
        return None
        
    total = find_sum_indices(str_so_far) % mod
    desired_value = mod - total
    if desired_value == mod:
        return 'a'
    elif desired_value > 25:
        return None
    else:
        # TODO: multiple modularly equivalent chars. e.g. a = b = c ... mod 1
        return chr(desired_value + ord('a'))
    

def generate_3l_strs_indices_div_by(num):
    valid_strs = []
    for c1 in range(26):
        for c2 in range(26):
            str_2l = "{}{}".format(index_to_chr(c1), index_to_chr(c2))
            c3 = char_that_completes_2l_str(num, str_2l)
            if c3:
                valid_strs.append("{}{}".format(str_2l, c3))

    return valid_strs

def main():
    print("73: {}".format(generate_3l_strs_indices_div_by(73)))
    for x in range(11):
        print("{}: {}".format(x*10, generate_3l_strs_indices_div_by(x*10)))
        if x == 0:
            print("no real number is divisible by 0")
    
    print("# such strs less than 10: 0", end="")
    for x in range(1, 101):
        if len(generate_3l_strs_indices_div_by(x)) < 10:
            print(", {}".format(x), end="")
    print()
    
    print("# such strs per number:")
    for x in range(1, 101):
        print("{}: {}".format(x, len(generate_3l_strs_indices_div_by(x))))
    

main()
