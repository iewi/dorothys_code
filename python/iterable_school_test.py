from iterable_school import IterableHigh, IterableMiddle, IterableElementary

high = IterableHigh()
middle = IterableMiddle()
elementary = IterableElementary()

print("high school name: {}".format(high.name))
print("middle school name: {}".format(middle.name))
print("elementary school name: {}".format(elementary.name))

high.name = "Woodrow Wilson High School"

print("new high school name: {}".format(high.name))

high.add_class("spanish 2")
high.add_class("algebra 1")

middle.add_class("pre-algebra")

elementary.add_class("3rd grade social studies")
elementary.add_class("2nd grade science")
elementary.add_class("5th grade history")

print("high school classes: {}".format(high.list_classes()))
print("middle school classes: {}".format(middle.list_classes()))
print("elementary classes: {}".format(elementary.list_classes()))

print("iterating through high school classes... ", end="")
for cl in high:
    print(cl, end=", ")
print()

print("iterating through middle school classes... ", end="")
for cl in middle:
    print(cl, end=", ")
print()

print("iterating through elementary school classes... ", end="")
for cl in elementary:
    print(cl, end=", ")
print()
