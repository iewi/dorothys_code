import sys
import string

def is_word(arg_string):
    for ch in arg_string:
        if ch in string.ascii_letters:
            return True
    return False

def stat(file_to_analyze):
    line_ct = 0
    word_ct = 0
    max_word_len = 0
    words = {} # the idea is that each word is a key associated with its frequency
   
    for line in file_to_analyze:
        line_ct += 1
        line = line.split("\n")[0]
        words_in_line = line.split(" ")
        for word in words_in_line:
            if is_word(word):
                word_ct += 1
                
                if len(word) > max_word_len:
                    max_word_len = len(word)
                
                if word in words.keys():
                    words[word] += 1
                else:
                    words[word] = 1

    print("line count of file is: {}".format(line_ct))
    print("word count of file is: {}".format(word_ct))
    print("longest word is {} characters long".format(max_word_len))
    print("word frequency is: {}".format(words))

def main():
    if len(sys.argv) < 2:
        print("please provide a file name to produce statistics on")
        return
    
    with open(sys.argv[1], "r") as arg_file:
        stat(arg_file)
    
    return

main()
