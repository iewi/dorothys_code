from better_class_list import ClassList

class IterableSchool:
    def __init__(self):
        self._class_list = ClassList()
        self.name = ""
    
    def __str__(self):
        print("{}".format(self.name))
    
    def add_class(self, class_name):
        self._class_list.add_class(class_name)
    
    def list_classes(self):
        return self._class_list.list_classes()
    
    def __iter__(self):
        self._class_list.__iter__()
        return self
    
    def __next__(self):
        return self._class_list.__next__()

class IterableHigh(IterableSchool):
    def __init__(self):
        super(IterableHigh, self).__init__()
        self.name = "High School"
        
class IterableMiddle(IterableSchool):
    def __init__(self):
        super(IterableMiddle, self).__init__()
        self.name = "Middle School"
        
class IterableElementary(IterableSchool):
    def __init__(self):
        super(IterableElementary, self).__init__()
        self.name = "Elementary School"
