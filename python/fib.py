# dorothy carter - august 2020
# for pre-core training - calculate first 20 fibonacci digits

def main():
    # seed with the first two digits
    fib_digits = [1, 1]
    
    # then only need 18 more
    for i in range(18):
        # the indices -1 and -2 refer to the last and
        # 2nd-to-last elements, respectively
        fib_digits.append(fib_digits[-1] + fib_digits[-2])
    
    print(fib_digits)

main()
