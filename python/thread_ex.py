import threading
import multiprocessing
import time

def single_thread_pow(x, y):
    for i in range(x, y+1):
        pow(i, i)
        #print("computed {}^{}".format(i, i), end = " ")
    print()

def multithread_pow(n, max_threads):
    # divide up range
    num_threads = n
    amt_per_thread = 1
    if n > max_threads:
        num_threads = max_threads
        amt_per_thread = n // max_threads + 1
    
    thread_list = []
    for i in range(num_threads):
        print("spawn thread range {} - {}".format(i*amt_per_thread + 1, (i+1)*amt_per_thread))
        thr = threading.Thread(target=single_thread_pow, args=(i*amt_per_thread + 1, min((i+1)*amt_per_thread, n)))
        thread_list.append(thr)
        thr.start()
    
    for thr in thread_list:
        thr.join()

def main(num_cores):
    now = time.time()
    single_thread_pow(1, 1000)
    after = time.time()
    print("single thread took {}s".format(after - now))
    
    
    now = time.time()
    multithread_pow(1000, 64)
    after = time.time()
    print("multi thread took {}s".format(after - now))
    
    with multiprocessing.Pool(num_cores) as pool:
        now = time.time()
        pool.reduce(multithread_pow, )
        after = time.time()
    print("multi processing took {}s".format(after - now))

main(8)
