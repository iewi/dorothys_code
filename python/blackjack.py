import random

class card:
    def __init__(self, value="", suit=""):
        self.suit = suit
        self.value = value
    
    def numeric_value(self):
        if self.value in ["2", "3", "4", "5", "6", "7", "8", "9", "10"]:
            return int(value)
        elif self.value in ["A", "K", "Q", "J"]:
            return 11
    
    def __str__(self):
        return "{}{}".format(self.value, self.suit)
    
    def __repr__(self):
        return "{}{}".format(self.value, self.suit)
            
    def generate_shuffled_deck():
        deck = []
        for suit in ["♥", "♠", "♣", "♦"]:
            for value in ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]:
                deck.append(card(value, suit))
        random.shuffle(deck)
        return deck
        
            
class player:
    def __init__(self, name=""):
        self.hand = []
        self.name = name
        
    def __str__(self):
        return self.hand
        
    def hand_value(self):
        total = 0
        for card in self.hand:
            total += card.numeric_value()
         
        return total

    def did_i_win(self, dealer, other_players):
        if self.hand_value() > 21:
            return False
        
        elif self.hand_value() == 21:
            return True
        
        elif self.hand_value() > dealer.hand_value():
            return False
        
        else:
            for player in other_players:
                if self.hand_value() < player.hand_value():
                    return False
            
            return True
    

class dealer(player):
    def __init__(self, name=""):
        self.hand = []
        self.deck = []
        self.name = "Dealer"
        
    def __str__(self):
        return "{}".format(deck[0])
        
    def deal(self, player_list):
        deck = card.generate_shuffled_deck()
        for p in player_list:
            p.hand.append(deck.pop())
            p.hand.append(deck.pop())
        
        self.deck = deck
   
    def hit(self, player):
        player.hand.append(deck.pop())


def get_cli_action(human_player, dealer):
    print("here is your hand:")
    print(human_player.hand)
    print("it has a calculated numeric value of {}".format(human_player.hand_value())
    print()
    response = input("would you like to hit or stay? type either hit or stay")
    if "h" in response:
        dealer.hit(human_player)
        return
    elif "s" in response:
        return
    else:
        print("did not understand. please try again.")
        while "h" not in response and "s" not in response:
            response = input("would you like to hit or stay?")
        
        if "h" in response:
            dealer.hit(human_player)
        
        return

def game_init(num_players, human_name):
    d = dealer()
    h = player(human_name)
    c = []
    for i in range(num_players - 1):
        c.append(player("ai {}".format(i)))
    
    all_players = [h]
    all_players.extend(c)
    
    d.deal(all_players)
    return (d, h, c)

def initial_cli():
    print("blackjack!")
    name = input("please enter your name")
    return name
    
def computer_action(c_player, dealer):
    if c_player.hand_value() < 15:
        dealer.hit(c_player)
    return

def dealer_action(dealer):
    if dealer.hand_value() < 17:
        dealer.hit(dealer)

def who_won(dealer, player_list):
    winners = []
        
    for player in player_list:
        other_players = player_list
        other_players.remove(player)
        if player.did_i_win(dealer, other_players):
            winners.append(player)
        
    if dealer.did_i_win(dealer, player_list):
        winners.append(dealer)
    
    return winners

def game_round(all_players):
    (dealer, human, player_list) = all_players
    get_cli_action(human, dealer)
    for c_player in player_list:
        computer_action(c_player, dealer)
    dealer_action(dealer)
    return

def finish(all_players):
    for p in all_players:
        print("player {}'s hand {} has the numeric value {}.".format(p.name, p.hand, p.hand_value()))
    
    winners = who_won(all_players[0], all_players[1:])
    print("these people won: {}".format(winners))
